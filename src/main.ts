import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/assets/css/main.scss'
import 'vue-toast-notification/dist/theme-default.css'

import feather from 'vue-icon'
import VueToast from 'vue-toast-notification'

Vue.use(VueToast)
Vue.use(feather, 'v-icon')
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
