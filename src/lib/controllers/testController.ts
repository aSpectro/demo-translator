export class TestController{
  private randomWords: Array<any>
  private words: Array<any>

  constructor(words: Array<any>){
    this.randomWords = []
    this.words = words
  }

  private getVariables(): void{
    this.randomWords.map((word: any) => {
      let variables = []
      const words = Object.assign([], this.words)
      variables = words.filter((f: any) => f.uuid !== word.uuid)
      variables = variables.map((v: any) => {
        return v.translate
      })
      variables = this.getRandomElements(variables, 5)
      variables.push(word.translate)
      variables = this.shuffleArray(variables)
      word.variables = variables
      
      return word
    })
  }

  private shuffleArray(arr: any): any{
    for (let i = arr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))
      const temp = arr[i]
      arr[i] = arr[j]
      arr[j] = temp
    }

    return arr
  }

  private getRandomElements(arr: any, n: number) {
    const result = []
    const selfArr = Object.assign([], arr)

    for (let i = 0; i < n; i++) {
      const index = Math.floor(Math.random()*selfArr.length)
      result.push(selfArr[index])
      selfArr.splice(index, 1)
    }
    return result
}

  public getTest(): Array<any>{
    const selfArr = Object.assign([], this.words)

    for (let i = 0; i < 20; i++) {
      const index = Math.floor(Math.random()*selfArr.length)
      this.randomWords.push(selfArr[index])
      selfArr.splice(index, 1)
    }

    this.getVariables()

    return this.randomWords
  }
}