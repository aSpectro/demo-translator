import * as interfaces from "@/lib/interfaces"

export class Validator{
  public validate(word: interfaces.word): boolean{
    let result = false

    const trimmedOriginal = word.original.trim()
    const trimmedTranslate = word.translate.trim()

    if(trimmedOriginal.length > 0 && trimmedTranslate.length > 0){
      result = true
    } else{
      result = false
    }

    return result
  }

  public formatWord(word: string): string{
    const lower = word.toLowerCase()
    const fUp = lower.charAt(0).toUpperCase()+lower.slice(1)
    
    return fUp
  }
}