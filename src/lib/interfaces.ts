export interface word{
  uuid?: string,
  original: string,
  translate: string
}