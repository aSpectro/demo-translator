import Vue from 'vue'
import Vuex from 'vuex'

// modules
import Words from '@/store/modules/words'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    
  },

  mutations: {

  },

  actions: {

  },

  modules: {
    Words
  }
})
