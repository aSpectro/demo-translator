declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module 'vue-icon'
declare module 'uuid'
declare module 'vue-messages'