import * as interfaces from "@/lib/interfaces"

const words: Array<interfaces.word> = [
  {
    uuid: '1',
    original: 'Hello',
    translate: 'Привет'
  },
  {
    uuid: '2',
    original: 'Morging',
    translate: 'Утро'
  },
  {
    uuid: '3',
    original: 'Tea',
    translate: 'Чай'
  },
  {
    uuid: '4',
    original: 'Cat',
    translate: 'Кот'
  },
  {
    uuid: '5',
    original: 'Dog',
    translate: 'Собака'
  },
  {
    uuid: '6',
    original: 'List',
    translate: 'Список'
  },
  {
    uuid: '7',
    original: 'Sun',
    translate: 'Солнце'
  },
  {
    uuid: '8',
    original: 'Tree',
    translate: 'Дерево'
  },
  {
    uuid: '9',
    original: 'Grass',
    translate: 'Трава'
  },
  {
    uuid: '10',
    original: 'Door',
    translate: 'Дверь'
  },
  {
    uuid: '11',
    original: 'Bread',
    translate: 'Хлеб'
  },
  {
    uuid: '12',
    original: 'Cheese',
    translate: 'Сыр'
  },
  {
    uuid: '13',
    original: 'Window',
    translate: 'Окно'
  },
  {
    uuid: '14',
    original: 'Table',
    translate: 'Стол'
  },
  {
    uuid: '15',
    original: 'Fox',
    translate: 'Лиса'
  },
  {
    uuid: '16',
    original: 'Button',
    translate: 'Кнопка'
  },
  {
    uuid: '17',
    original: 'Place',
    translate: 'Место'
  },
  {
    uuid: '18',
    original: 'Forest',
    translate: 'Лес'
  },
  {
    uuid: '19',
    original: 'Keyboard',
    translate: 'Клавиатура'
  }
]

export default words