import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import * as interfaces from "@/lib/interfaces"
import { v4 as uuidv4 } from 'uuid'

import wordsMock from '@/store/mocks/words'

@Module({ namespaced: false })
class Words extends VuexModule{

  public items: Array<interfaces.word> = wordsMock

  get words(): Array<interfaces.word>{
    return this.items
  }

  @Mutation
  public updateWord(updatableWord: interfaces.word): void{
    this.items = this.items.map((word: interfaces.word) => {
      if(word.uuid === updatableWord.uuid){
        word = updatableWord
      }
      
      return word
    })
  }

  @Mutation
  public setWord(newWord: interfaces.word): void{
    newWord.uuid = uuidv4()
    this.items.push(newWord)
  }

  @Mutation
  private setWords(words: Array<interfaces.word>): void{
    this.items = words
  }

  @Action({ rawError: true })
  public deleteWord(wordUuid: string): void{    
    const newWords = this.items.filter((word) => word.uuid !== wordUuid)
    this.context.commit('setWords', newWords)
  }
}
export default Words